<?php

include_once "includes/dbconnect.php";

if(isset($_GET['l']) && isset($_GET['u'])){
    $sql = "INSERT INTO links (list, url) VALUES (?, ?)";
    $prep = $mysqli -> prepare($sql);
    $prep -> bind_param("ss", $_GET['l'], $_GET['u']);
    $prep -> execute();
}