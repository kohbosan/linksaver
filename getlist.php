<?php

include_once "includes/dbconnect.php";

if(isset($_GET['l'])){
    $sql = "SELECT url FROM links WHERE list = ?";
    $prep = $mysqli -> prepare($sql);
    $prep -> bind_param("s", $_GET['l']);
    $prep -> execute();

    $prep -> bind_result($url);
    while($prep -> fetch()){
        $result[] = $url;
    }

    echo json_encode($result, JSON_PRETTY_PRINT);
}