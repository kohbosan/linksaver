<!doctype html>

<html>
<?php include_once "includes/head.php"; ?>
<body>
<div class="container">
    <div id="form">
        <form class="col m4 s12">
            <div class="input-field col s12">
                <input type="text" id="listbox" onkeypress="return handleInput(event)"/>
                <label for="listbox">List</label>
            </div>
        </form>
    </div>
    <div id="links" class="col m6 s12 offset-m2"></div>
</div>
</body>
<?php include_once "includes/scripts.php"; ?>
</html>