<?php

include_once "includes/dbconnect.php";

if(isset($_GET['l']) && isset($_GET['u'])){
    $sql = "DELETE FROM links WHERE list=? AND url=?";
    if($prep = $mysqli -> prepare($sql)){
        $prep -> bind_param("ss", $_GET['l'], $_GET['u']);
        $prep -> execute();
    } else {
        echo "Error preparing statement";
    }
}