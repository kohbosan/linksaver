function getList(list){
    xhr = new XMLHttpRequest();
    xhr.open("GET", "http://kohding.net/links/getlist.php?l=" + list);
    xhr.send();

    xhr.onreadystatechange = function(){
        if(xhr.readyState === 4){
            showLinks(list, JSON.parse(xhr.responseText));
        }
    }
}

function showLinks(list, arr){
    var links = arr;
    var linksdiv = document.getElementById("links");
    linksdiv.innerText = "";
    if(links != null){
        for(var i = 0; i < links.length; i++){
            var div = document.createElement("div");
            var a = document.createElement("a");
            a.innerText = links[i];
            a.href = links[i];
            div.appendChild(a);

            var x = document.createElement("span");
            x.innerText = "[x]";
            x.onclick = function(){ removeLink(list, encodeURIComponent(a.innerText)) };
            div.appendChild(x);
            linksdiv.appendChild(div);
        }
    }
    var addButton = document.createElement("p");
    addButton.innerText = "[+]";
    addButton.id = "addButton";
    addButton.style.margin = "0";
    addButton.onclick = function(){switchAddToListBox(showAddToListBox)};
    linksdiv.appendChild(addButton);
    var listAddBox = document.createElement("input");
    listAddBox.type = "text";
    listAddBox.id = "listAddBox";
    listAddBox.style.display = "none";
    listAddBox.setAttribute("data-list", list);
    listAddBox.onkeydown = handleAddToList;
    linksdiv.appendChild(listAddBox);
}

function handleInput(e){
    var input = document.getElementById("listbox");
    if(e.which == 13 || e.keyCode == 13){
        e.preventDefault();
        getList(encodeURIComponent(input.value));
        return false;
    }
}

function handleAddToList(e){
    var urlbox = document.getElementById("listAddBox");
    if(e.which == 13 || e.keyCode == 13){
        e.preventDefault();
        addToList(urlbox.dataset.list, encodeURIComponent(urlbox.value));
        return false;
    }
}

function addToList(list, url){
    xhr = new XMLHttpRequest();
    xhr.open("GET", "http://kohding.net/links/addlink.php?l=" + list + "&u=" + url);
    xhr.send();

    xhr.onreadystatechange = function(){
        if(xhr.readyState === 4){
            getList(list);
        }
    }
}

function switchAddToListBox(callback){
    var addButton = document.getElementById("addButton");
    addButton.style.display = "none";
    callback();
}

function showAddToListBox(){
    var listAddBox = document.getElementById("listAddBox");
    listAddBox.style.display = "inline";
}

function removeLink(list, url){
    xhr = new XMLHttpRequest();
    xhr.open("GET", "http://kohding.net/links/removelink.php?l=" + list + "&u=" + url);
    xhr.send();

    xhr.onreadystatechange = function(){
        if(xhr.readyState === 4){
            getList(list);
        }
    }
}